export const calcGrid = (baseHeight: number, coeff: number) => {
    return baseHeight / coeff;
};
