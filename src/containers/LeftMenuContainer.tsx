import * as React from "react";
import "./LeftMenu.css"

interface ILeftMenuContainerProps {
    githubLink?: string;
    linkedinLink?: string;
    children: React.ReactChild[];
}

export const LeftMenuContainer = (props: ILeftMenuContainerProps) =>
    <div className="left-menu-container transitive bgImg">
        { ...props.children }
    </div>;