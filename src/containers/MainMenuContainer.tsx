import * as React from "react"
import {MainMenuItem} from "../components/MainMenu/MainMenuItem";
import {MainMenuWrapper} from "../components/MainMenu/MainMenuWrapper";

import "./MainMenu.css";

export const MainMenuContainer = (props: {items: string[]}) =>
  <MainMenuWrapper>
    { props.items.map(e => <MainMenuItem value={e} key={e}/>) }
  </MainMenuWrapper>;
