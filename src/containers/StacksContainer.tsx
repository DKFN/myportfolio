import * as React from "react";
import {StackComponent} from "../components/Stack/StackComponent";
import "./Stacks.css";

interface IStacksContainerProps {
  stacks: any[];
}

interface IStacksContainerState {
  currentStack: number;
}

export class StacksContainer extends React.Component<IStacksContainerProps, IStacksContainerState> {
  constructor(props: IStacksContainerProps) {
    super(props);
    this.state = {
      currentStack: 0,
    };
    this.onChangeStack = this.onChangeStack.bind(this);
  }

  public render() {
    return (
      <div className="stack-view-container">
        <h1>Stacks I'm in !</h1>
        {
          this.props.stacks.map((e, n) =>
            <StackComponent stack={e}
                            isActive={n === this.state.currentStack}
                            key={n}/>)
        }
      </div>
    );
  }

  private onChangeStack(nextIdx: number) {
    this.setState({
      currentStack: nextIdx,
    });
  }
}
