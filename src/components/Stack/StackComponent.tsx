import * as React from "react";
import {IStack} from "../../models/Stacks";
import {StackStrate} from "./StackStrate";

export const StackComponent = (props: {stack: IStack, isActive: boolean}) =>
  <div className="stack-item">
    <div className="stack-item-title">
      {props.isActive ? "!!" : ">>" } {props.stack.stackTitle} <br />
      <div className="stack-item-title-annotation">
        {props.stack.stackSubTitle}
      </div>
    </div>
    <StackStrate techs={props.stack.techsBackend} title={"Backend"} key={0}/>
    <StackStrate techs={props.stack.techsFrontend} title={"FrontEnd"} key={1}/>
  </div>;
