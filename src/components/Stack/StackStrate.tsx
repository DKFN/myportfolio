import * as  React from "react";

import {ITech} from "../../models/Techs";
import {TechItem} from "../Tech/TechItem";

export const StackStrate = (props: {techs: ITech[], title: string}) =>
  <div className="stack-item-tech-strate">
    <div className="stack-item-tech-title">
      <h2>{props.title}</h2>
    </div>
    <div className="stack-item-tech-strate-inner-container">
      {
        props.techs.map((e, n) => <TechItem {...e} key={n}/>)
      }
    </div>
  </div>;
