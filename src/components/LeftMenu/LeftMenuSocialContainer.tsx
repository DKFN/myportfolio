import * as React from "react";

export const LeftMenuSocialContainer = () =>
<div className="left-menu-social-container">
    <div className="left-menu-little-bio">
        <div className="left-menu-bio-phrases">
                            <span style={{fontSize: 8}}>
                                (not me)
                            </span> <br /> <br /><br />
            Kinda crazy Fullstack Scala && Typescript developper. <br /> <br />
            <span style={{fontSize: 8}}>
                May bite if you argue about Scala awesomeness. <br /> <br />
            </span>
        </div>
        <b>Age : </b> 23 <br />
    </div>
    <span className="menu-left-icon">
                        <i className="fab fa-github"/>
                    </span>
    <span className="menu-left-icon">
                        <i className="fab fa-linkedin"/>
                    </span>
    &nbsp;
</div>;
