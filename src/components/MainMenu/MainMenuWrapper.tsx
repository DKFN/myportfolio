import * as React from "react";

export const MainMenuWrapper = (props: {children: any[]}) =>
  <div className="main-menu-large-layout transitive">
      { ...props.children }
  </div>;
