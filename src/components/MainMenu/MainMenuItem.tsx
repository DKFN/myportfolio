import * as React from "react";

export const MainMenuItem = (props: {value: string}) =>
<div className="main-menu-element transitive">
    { props.value }
</div>;
