import * as _ from "lodash";
import * as React from "react";

const levelTexts = [
  "null",
  "Newbie",
  "Support",
  "Carry",
  "Main",
  "Rampage"
];

export const TechSkill = (props: {level: number}) =>
  <span className="stack-overall-skill">
    {levelTexts[props.level]} &nbsp;
    {
      _.range(props.level).map((x, n) => <i className="fa fa-star" aria-hidden="true" key={n}/>)
    }
  </span>;
