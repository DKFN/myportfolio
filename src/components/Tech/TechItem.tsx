import * as React from "react";

import {ITech} from "../../models/Techs";
import {TechSkill} from "./TechSkill";

export const TechItem = (props: ITech) =>
  <div className="stack-item-tech">
    <img src={props.logo} /><br />
    <span className="stack-tech-name">
      {props.title}
    </span><br />
    <TechSkill level={props.level}/>
    <div className="stack-overall-description">
      {props.description}
    </div>
  </div>;
