import * as React from "react";

export const ViewLayout = (props: {children: React.ReactChild}) =>
    <div className="view-container">
        {props.children}
    </div>;

