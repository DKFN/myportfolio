import * as React from 'react';
import './App.css';

/* tslint:disable */
import {LeftMenuAvatar} from "./components/LeftMenu/LeftMenuAvatar";
import {LeftMenuBottom} from "./components/LeftMenu/LeftMenuBottom";
import {LeftMenuContainer} from "./containers/LeftMenuContainer";
import {LeftMenuSocialContainer} from "./components/LeftMenu/LeftMenuSocialContainer";
import {MainMenuContainer} from "./containers/MainMenuContainer";
import {menuItems} from "./models/MenuItems";
import {StacksContainer} from "./containers/StacksContainer";
import {mainStack} from "./models/Stacks";
/* tslint:enable */

const App = () =>
<div className="App" style={{height: window.innerHeight}}>
  <LeftMenuContainer >
    <LeftMenuAvatar />
    <LeftMenuSocialContainer />
    <LeftMenuBottom />
  </LeftMenuContainer>
  <div className="main-content-container">
  <MainMenuContainer items={menuItems}/>
    <StacksContainer stacks={[mainStack]}/>
  </div>
</div>;

export default App;
