
import {
  akkaTech, cassandraTech, ESTech, GraphQLTech, ITech, playTech, ReactTech, redisTech, ReduxTech, sangriaTech,
  scalaTech
} from "./Techs";
// import TypescriptLogo from "./images/stacks/typescript.png";

/* tslint:disable*/
export interface IStack {
  stackTitle: string;
  stackSubTitle: string;
  techsBackend: ITech[];
  techsFrontend: ITech[];
}

// Stacks
export const mainStack: IStack = {
  stackTitle: "Main Stack",
  stackSubTitle: "This is the stack I use daily at work and really love.",
  techsBackend: [scalaTech, playTech, sangriaTech, cassandraTech, redisTech, ESTech, akkaTech],
  techsFrontend: [ReactTech, GraphQLTech, ReduxTech],
};
