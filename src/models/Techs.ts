import Akka from "../images/stacks/akka.png";
import Cassandra from "../images/stacks/cassandra.png";
import ESLogo from "../images/stacks/elatic.png";
import GraphQL from "../images/stacks/graphql.png";
import Kafka from "../images/stacks/kafka-logo.png";
import Play from "../images/stacks/play.png";
import ReactLogo from "../images/stacks/react.png"
import Redis from "../images/stacks/redis.png";
import ReduxLogo from "../images/stacks/redux.png";
// import Relay from "../images/stacks/relay.png";
import Sangria from "../images/stacks/sangria-logo.png";
import Scala from "../images/stacks/scala.png";

// Interfaces
export interface ITech {
  description?: string;
  level: number;
  logo: string;
  title: string;
}

// Raw Data
/* tslint:disable */
export const scalaTech: ITech = {
  title: "Scala",
  level: 3,
  logo: Scala,
  description: "I have been working with Scala for almost a year and I absolutely love it, event if Scala and Functionnal takes more than this to master."
};

export const playTech: ITech = {
  title: "Play! Framework",
  level: 3,
  logo: Play,
  description: "Since I use Scala, I use Play Framework. I find it wonderful at work and for personnal projects. Even if nothing is full heaven (damn filters !)"
};

export const sangriaTech: ITech = {
  title: "Sangria",
  level: 3,
  logo: Sangria,
  description: "Sangria is the library we use for our GraphQL middleware, I often maintain it because I use GraphQL a lot"
};

export const cassandraTech: ITech = {
  title: "Cassandra",
  level: 3,
  logo: Cassandra,
  description: "Cassandra is my favorite nosql DB system but it must be noted that I know nothing about setting it up for proper scaling."
};


export const akkaTech: ITech = {
  title: "Akka",
  level: 1,
  logo: Akka,
  description: "Akka is used in critical parts of the app. I only know the basics about Akka yet."
};

export const redisTech: ITech = {
  title: "Redis",
  level: 2,
  logo: Redis,
  description: "I use Redis at work and have mainted some code using it, but I have never done something new with it yet."
};

export const ESTech: ITech = {
  title: "ElasticSearch",
  level: 2,
  logo: ESLogo,
  description: "I use elastic search at work but alike Redis i did not take a deep dive into this technology",
};

export const KafkaTech: ITech = {
  title: "Kafka",
  level: 2,
  logo: Kafka,
  description: ""
};

export const ReactTech: ITech = {
  title: "React",
  level: 4,
  logo: ReactLogo,
  description: "I have been using for 1 1/2 year from now and I keep using it evrywhere ! (And I didn't like it at first look, what a fool !)"
};

export const GraphQLTech: ITech = {
  title: "GraphQL",
  level: 4,
  logo: GraphQL,
  description: "I'm working with GraphQL both frotend and backend daily, it is a the center of our API and is the #1 choice gateway for our clients now."
};

export const ReduxTech: ITech = {
  title: "Redux",
  level: 3,
  logo: ReduxLogo,
  description: "Sometimes you have UIs without thousands of events and you need Redux ! It is the great savior of Backend's control UI."
};
